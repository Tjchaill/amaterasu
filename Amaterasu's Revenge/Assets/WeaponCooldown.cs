﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCooldown : MonoBehaviour
{
    public float cooldownTime = 2;

    private float nextFireTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        if(Time.time > nextFireTime)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                print("ability used, cooldown started");
                nextFireTime = Time.time + cooldownTime;
            }
        }
    }
}
