﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]

public class PlayerJump : MonoBehaviour
{
    //force, apply force, 1x
    //rb.velocity = new Vector2(rb.velocity.x, jumpForce);

    [Header("Public Vars")]
    public float jumpForce = 17.0f;
    
    private Rigidbody2D rb;
    private Animator myAnimator;
    
    

    [Header("Private Vars")]
    [SerializeField] private Transform groundcheck;
    [SerializeField] private float radOCircle;
    [SerializeField] private LayerMask whatIsGround;
    public bool grounded;

    //myAnimator.SetBool("landing", true);

    //myAnimator.SetBool("landing", false);

    //myAnimator.SetTrigger("jump");

    //

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        
    }

    private void Update()
    {
        grounded = Physics2D.OverlapCircle(groundcheck.position,radOCircle,whatIsGround);

        if (grounded)
        {
            myAnimator.SetBool("landing", false);
            myAnimator.ResetTrigger("jump");
            
        }

        

        if(Input.GetButtonDown("Jump") && grounded)
        {
            SoundManagerScript.PlaySound("Jump");
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            myAnimator.SetTrigger("jump");

        }

        if (Input.GetButtonUp("Jump"))
        {

            myAnimator.SetBool("landing", true);
            myAnimator.ResetTrigger("jump");
        }

        if(rb.velocity.y < 0)
        {
            myAnimator.SetBool("landing", true);
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(groundcheck.position, radOCircle);
    }

    private void FixedUpdate()
    {
        HandleLayers();
    }

    private void HandleLayers()
    {
        if (!grounded)
        {
            myAnimator.SetLayerWeight(1, 1);
        }

        else
        {
            myAnimator.SetLayerWeight(0, 0);
        }
    }
}
