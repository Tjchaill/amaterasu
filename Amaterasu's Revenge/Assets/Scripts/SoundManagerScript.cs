﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{

    public static AudioClip AttackSound, JumpSound, DamageSound, EnemyGrunt;
    static AudioSource audioSrc;

    // Start is called before the first frame update
    void Start()
    {
        AttackSound = Resources.Load<AudioClip>("Attack");
        JumpSound = Resources.Load<AudioClip>("Jump");
        DamageSound = Resources.Load<AudioClip>("Damage");
        EnemyGrunt = Resources.Load<AudioClip>("Grunt");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "Attack":
                audioSrc.PlayOneShot(AttackSound);
                break;
            case "Jump":
                audioSrc.PlayOneShot(JumpSound);
                break;
            case "Damage":
                audioSrc.PlayOneShot(DamageSound);
                break;
            case "Grunt":
                audioSrc.PlayOneShot(EnemyGrunt);
                break;
        }

    }
       
}
