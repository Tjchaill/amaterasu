﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]

public class PlayerMovement : MonoBehaviour
{
    // necessary for animations and physics
    private Rigidbody2D rb2D;
    private Animator myAnimator;

    private bool facingRight = true;

    // variables to play with
    public float speed = 2.0f;
    public float horizMovement;
    public bool IsDead;
     

    public GameObject gameOverText, restartButton;

    // Start is called before the first frame update
    private void Start()
    {
        gameOverText.SetActive(false);
        restartButton.SetActive(false);
       

        // define the gameobjects found on the player
        rb2D = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame, handles input for physics
    private void Update()
    {
        //check if the player has input movement
        horizMovement = Input.GetAxis("Horizontal");
        if (Input.GetButtonDown("Fire1"))
        {
            myAnimator.SetBool("IsShooting", true);
        }

        if (Input.GetButtonUp("Fire1"))
        {
            myAnimator.SetBool("IsShooting", false);
        }

    }

    //handles running the physics
    private void FixedUpdate()
    {
        //move the character
        rb2D.velocity = new Vector2(horizMovement*speed, rb2D.velocity.y);

        myAnimator.SetFloat("speed", Mathf.Abs(horizMovement));

        Flip(horizMovement);
    }

    // flipping function
    private void Flip(float horizontal)
    {
        if(horizontal < 0 && facingRight || horizontal > 0 && !facingRight)
        {
            facingRight = !facingRight;

            transform.Rotate(0f, 180f, 0f);
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Enemy"))
        {
            gameOverText.SetActive(true);
            restartButton.SetActive(true);
            myAnimator.SetBool("IsDead", true);
            SoundManagerScript.PlaySound("Damage");
            Destroy(gameObject, 1f);

        }
        if (col.gameObject.tag.Equals("Spike"))
        {
            gameOverText.SetActive(true);
            restartButton.SetActive(true);
            myAnimator.SetBool("IsDead", true);
            SoundManagerScript.PlaySound("Damage");
            Destroy(gameObject, 1f);

        }
        if (col.gameObject.tag.Equals("Ball"))
        {
            gameOverText.SetActive(true);
            restartButton.SetActive(true);
            myAnimator.SetBool("IsDead", true);
            SoundManagerScript.PlaySound("Damage");
            Destroy(gameObject, 1f);
        }

    }

}
