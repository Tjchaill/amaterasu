﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kunai : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * speed;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Environment"))
        {
            Destroy(gameObject);
        }
        if (col.gameObject.tag.Equals("Ball"))
        {
            Destroy(gameObject);
        }
    }

}
