﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public Transform firePoint;
    public GameObject Kunai;
    public Animator myAnimator;

    public bool Cooldown;

    // Start is called before the first frame update
    void Start()
    {
        Cooldown = true;
        myAnimator.SetBool("CooledDown", false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();  
        }            
    }

    private void Shoot()
    {
        if (Cooldown)
        {
            Instantiate(Kunai, firePoint.position, firePoint.rotation);
            SoundManagerScript.PlaySound("Attack");
            myAnimator.SetBool("IsShooting", true);
            Cooldown = false;
            myAnimator.SetBool("CooledDown", true);
            StartCoroutine("wait");
        }

    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Ground"))
        {
            Destroy(gameObject);
        }
        
    }

    public IEnumerator wait()
    {
        yield return new WaitForSeconds(1);
        Cooldown = true;
        myAnimator.SetBool("CooledDown", false);
    }

   
           
           

}